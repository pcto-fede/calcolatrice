import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CalcService {
  operation(
    op: string,
    a: number,
    b: number,
    isOnline: boolean | undefined
  ): ReplaySubject<number> {
    let tot: ReplaySubject<number> = new ReplaySubject();

    if (isOnline) {
      //if isOnline, then call the api
      op = op.replace(/\+/g, '%2B').replace(/\//g, '%2F');
      console.log(`http://api.mathjs.org/v4/?expr=${a}${op}${b}`);
      this.http
        .get<string>(`http://api.mathjs.org/v4/?expr=${a}${op}${b}`)
        .subscribe((data) => {
          tot.next(parseInt(data));
          console.log(data);
        });

      //wait the response
      return tot;
    } else {
      //if not isOnline, then use this implementation
      switch (op) {
        case '+':
          tot.next(a + b);
          break;
        case '-':
          tot.next(a - b);
          break;
        case '*':
          tot.next(a * b);
          break;
        case '/':
          tot.next(a / b);
          break;
        default:
          tot.next(0);
          break;
      }
      return tot;
    }
  }

  constructor(private http: HttpClient) {}
}
