import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcNorpnComponent } from './calc-norpn.component';

describe('CalcNorpnComponent', () => {
  let component: CalcNorpnComponent;
  let fixture: ComponentFixture<CalcNorpnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalcNorpnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcNorpnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
