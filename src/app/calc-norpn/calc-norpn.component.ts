import { Component, OnInit } from '@angular/core';
import { CalcService } from '../calc.service';

@Component({
  selector: 'app-calc-norpn',
  templateUrl: './calc-norpn.component.html',
  styleUrls: ['./calc-norpn.component.css'],
})
export class CalcNorpnComponent implements OnInit {
  line = ''; //line of the display
  stack: string[] = []; //stack of operations
  stackView = ''; //the view of the stack (for html template)
  toResetDisplay = false; //if display needs reset (after displaying a result)
  isOnline = false;

  //add number to display
  addToLine(n: number): void {
    //if error or a previews result is shown, the display needs a reset
    if (isNaN(parseInt(this.line)) || this.toResetDisplay) {
      this.line = '';
      this.toResetDisplay = false;
    }
    this.line += n;
  }

  //clear the display (when CRL LN is pressed)
  clearLine(): void {
    this.line = '';
  }

  //clear the stack and the display (when CRL ALL is pressed)
  clearStack(): void {
    this.stack = [];
    this.clearLine();
    this.refreshStackView();
  }

  //sync stackView with the content of stack
  refreshStackView(): void {
    this.stackView = '';
    for (let i in this.stack) {
      this.stackView += i + '➤ ' + this.stack[i] + '\n ';
    }
  }

  //if the line isn't an error, push the line to the stack
  pushLineToStack(): void {
    if (this.line && !isNaN(parseInt(this.line))) {
      console.log('pushed: ' + this.line);
      this.stack.push(this.line);
      // this.line = '';
      this.refreshStackView();
    }
  }
  equalKey(): void {
    this.pushLineToStack();
    this.equal();
  }
  //do the last operation on the stack (if not bug, only one operation is present (3 elements))
  equal(): void {
    //only do operation if 3 elements are present
    if (this.stack.length >= 3) {
      let temp: string;
      let a: number;
      let b: number;
      let oper: string;
      let tot: number;

      //check if element is number
      temp = this.stack.pop() as string; //last number written
      if (!isNaN(parseInt(temp))) {
        b = parseInt(temp);
      } else {
        this.line = 'ERROR 1';
        return;
      }

      oper = this.stack.pop() as string; //operator

      //check if element is number
      temp = this.stack.pop() as string; //first number written
      if (!isNaN(parseInt(temp))) {
        a = parseInt(temp);
      } else {
        this.line = 'ERROR 2';
        return;
      }

      //do the operation
      this.calcService
        .operation(oper, a, b, this.isOnline)
        .subscribe((data) => {
          this.line = data.toString();
          this.stack.push(data.toString());
          this.refreshStackView;
        });

      //   this.line = tot.toString(); //display the result
      // this.stack.push(tot.toString()); //save the result (for another operation)
    } else {
      this.line = 'ERROR: stack piccolo';
    }
    this.refreshStackView();
  }

  //save operator and do the previews operation (when +|-|*|/ is pressed)
  operationOnStack(operator: string): void {
    //push current line
    this.pushLineToStack();
    this.refreshStackView();

    //if previews operation are present, do them
    if (this.stack.length >= 3) {
      this.equal();
    }
    this.toResetDisplay = true;

    //push the operator to the stack
    this.stack.push(operator);
    this.refreshStackView();
  }

  constructor(private calcService: CalcService) {}

  ngOnInit(): void {}
}
