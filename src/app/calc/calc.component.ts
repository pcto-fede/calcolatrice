import { CalcService } from './../calc.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.css'],
})
export class CalcComponent implements OnInit {
  line = '';
  stack: number[] = [];
  stackView = '';
  isOnline = false;
  //add number to display
  addToLine(n: number): void {
    //if error reset display
    // if (isNaN(parseInt(this.line))) {
    //   this.line = '';
    // }
    this.line += n;
  }

  //clear the display (when CRL LN is pressed)
  clearLine(): void {
    this.line = '';
  }

  //clear the stack and the display (when CRL ALL is pressed)
  clearStack(): void {
    this.stack = [];
    this.line = '';
    this.refreshStackView();
  }

  //if the line isn't an error, push the line to the stack
  pushToStack(): void {
    if (this.line && !isNaN(parseInt(this.line))) {
      console.log('pushed');
      this.stack.push(parseInt(this.line));
      this.line = '';
      this.refreshStackView();
    }
  }

  //sync stackView with the content of stack
  refreshStackView(): void {
    this.stackView = '';
    for (let i in this.stack) {
      this.stackView += i + '➤ ' + this.stack[i] + '\n ';
    }
  }

  //do operation with last two numbers (when +|-|*|/ is pressed)
  operationOnStack(operator: string): void {
    //only do operation if 2 elements are present
    if (this.stack.length >= 2) {
      let a = this.stack.pop() as number;
      let b = this.stack.pop() as number;
      let tot: number;

      //do the operation
      this.calcService
        .operation(operator, a, b, this.isOnline)
        .subscribe((data) => {
          this.line = data.toString();
          this.stack.push(data as number);
          this.refreshStackView;
        });

      //this.line = tot.toString(); //display the result
      //this.stack.push(tot); //save the result (for another operation)
    } else {
      this.line = 'ERROR';
    }
    this.refreshStackView();
  }

  constructor(private calcService: CalcService) {}

  ngOnInit(): void {}
}
