import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CalcComponent } from './calc/calc.component';
import { CalcNorpnComponent } from './calc-norpn/calc-norpn.component';

@NgModule({
  declarations: [AppComponent, CalcComponent, CalcNorpnComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
